AjaxUpload
========
>
@author：yangjian102621@gmail.com<br />

插件描述:
--------
javascript异步上传插件，包含3个子项目BUpload, JUpload, TUpload.
* BUpload : 基于HTML5， UI仿百度编辑器的图片上传, 支持图片上传，浏览图片，和图片搜索，支持图片预览，有进度条
* TUpload : 基于HTML5， UI仿腾讯的QQ空间上传图片，支持图片预览，有进度条。
* JUpload : 基于iframe的异步上传。


插件依赖:
-------
* jQuery-1.7.1以上版本

